//
//  sliderView.h
//  AntSecondHandCar
//
//  Created by wuhuyixiu on 16/2/22.
//  Copyright © 2016年 wuhuyixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SliderValueChange)(NSInteger min , NSInteger max, BOOL isMax);

@interface SliderView : UIView
-(id)initWithFrame:(CGRect)frame withLineCount:(NSInteger)lineCount bottomLineCount:(NSInteger)bottomLineLength;

- (void)setMin:(NSInteger)min max:(NSInteger)max;

@property (nonatomic, copy)SliderValueChange sliderValueChange;

@end
