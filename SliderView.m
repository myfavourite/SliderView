//
//  sliderView.m
//  AntSecondHandCar
//
//  Created by wuhuyixiu on 16/2/22.
//  Copyright © 2016年 wuhuyixiu. All rights reserved.
//

#import "SliderView.h"
#import "UIView+HDAddition.h"
@interface SliderView()
{
    CGFloat _lengthorgX;
    CGFloat _lengthorgY;
    CGFloat  lengthView_h;
    CGFloat  lengthView_y;
    
    //总共分成多少小段
    NSInteger  count;
    
    //一段代表值
    NSInteger bottomLength;
    CGFloat  Slider_length;
    
}
@property(nonatomic,strong)UIView *slview;
@property(nonatomic,strong)UIImageView *lfview;
@property(nonatomic,strong)UIImageView *rgview;
@property(nonatomic,strong)UIView *lengthView;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property(nonatomic,strong)UIPanGestureRecognizer *panRecognizer1;
@end

@implementation SliderView

#define Marge (8.0)

//#define Slider_length 

-(id)initWithFrame:(CGRect)frame withLineCount:(NSInteger)lineCount bottomLineCount:(NSInteger)bottomLineLength
{
    self=[super initWithFrame:frame];
    if(self)
    {
        self.backgroundColor = [UIColor whiteColor];

        _lengthorgX = self.HDview_h/2;
        _lengthorgY = self.HDview_w-self.HDview_h/2;
        bottomLength = bottomLineLength;
        count = lineCount;
        [self creatUI];
    }
    return self;
}
-(void)creatUI
{
    lengthView_h = 8;
    lengthView_y = (self.HDview_h - lengthView_h) / 2.0;
    Slider_length = ((self.HDview_w - self.HDview_h)/(CGFloat)(count + 1));
    
    if (bottomLength == 1) {
        Slider_length = ((self.HDview_w - self.HDview_h)/(CGFloat)(count));
    }
    
    UIView  *backLineView = [[UIView alloc]initWithFrame:CGRectMake(self.HDview_h/2, lengthView_y, self.HDview_w-self.HDview_h, lengthView_h)];
    backLineView.clipsToBounds = YES;
    backLineView.layer.cornerRadius = backLineView.HDview_h / 2.0;
    backLineView.backgroundColor = RGBA(240, 240, 240, 1);
    [self addSubview:backLineView];
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(self.HDview_h/2, self.HDview_h, self.HDview_w-self.HDview_h, 20)];
    [self addSubview:bottomView];
    
    NSInteger  bottomLabelCount = (NSInteger)(count / (bottomLength));
    for (int a = 0; a < bottomLabelCount + 1 ; a++) {
        UILabel  *label = [[UILabel alloc]initWithFrame:CGRectMake( -15 + (Slider_length * a * bottomLength) , 0, 30, bottomView.HDview_h)];    
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:12.0];
        label.textColor = [UIColor grayColor];
        label.text = [NSString stringWithFormat:@"%ld",(long)(bottomLength * a)];
        if (a == bottomLabelCount) {
            label.frame = CGRectMake( -15 + bottomView.HDview_w , 0, 30, bottomView.HDview_h);
            label.text = @"不限";
            label.textAlignment = NSTextAlignmentRight;
        }
        [bottomView addSubview:label];
    }
    
    _lfview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.HDview_h, self.HDview_h)];
    _lfview.userInteractionEnabled = YES;

    _rgview=[[UIImageView alloc]initWithFrame:CGRectMake(self.HDview_w-self.HDview_h, 0, self.HDview_h, self.HDview_h)];
    _rgview.userInteractionEnabled = YES;

    _lfview.tag=88;
    _rgview.tag=99;
    
    [_lfview setImage:[UIImage imageNamed:@"ant_slider_circle"]];
    [_rgview setImage:[UIImage imageNamed:@"ant_slider_circle"]];
    
    _lengthView=[[UIView alloc]initWithFrame:CGRectMake(self.HDview_h/2, lengthView_y, self.HDview_w-2*self.HDview_h/2, lengthView_h)];
    _lengthView.backgroundColor=THEME_COLOR;
    [self addSubview:_lengthView];
    [self addSubview:_lfview];
    [self addSubview:_rgview];
    
    
    
    _panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    _panRecognizer1 = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [_lfview addGestureRecognizer:_panRecognizer];
    [_rgview addGestureRecognizer:_panRecognizer1];
}

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    
    UIView  *view = recognizer.view;
    
    [self bringSubviewToFront:view];
    
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        if(view == _lfview)
        {
            CGPoint translatedPoint = [recognizer translationInView:_lfview];
            CGFloat x = recognizer.view.center.x + translatedPoint.x;
            CGFloat y = recognizer.view.center.y + 0;
            
            if(x<y)
            {
                NSLog(@"%f",y/2+2*Marge);
                recognizer.view.center = CGPointMake(y, y);
                _lengthorgX=y;
                
            }
            else if(x>self.HDview_w-y)
            {
                recognizer.view.center = CGPointMake(self.HDview_w-y, y);
                _lengthorgX=self.HDview_w-y;
            }
            else
            {
                recognizer.view.center = CGPointMake(x, y);
                _lengthorgX=x;
            }
            
            [recognizer setTranslation:CGPointMake(0, 0) inView:self];
        }
        if(view ==_rgview)
        {
            CGPoint translatedPoint = [recognizer translationInView:_rgview];
            CGFloat x = recognizer.view.center.x + translatedPoint.x;
            CGFloat y = recognizer.view.center.y + 0;
            
            if(x<y)
            {
                NSLog(@"%f",y/2+2*Marge);
                recognizer.view.center = CGPointMake(y, y);
                _lengthorgY=y;
            }
            else if(x>self.HDview_w-y)
            {
                recognizer.view.center = CGPointMake(self.HDview_w-y, y);
                _lengthorgY=self.HDview_w-y;
                
            }
            else
            {
                recognizer.view.center = CGPointMake(x, y);
                _lengthorgY=x;
            }
            [recognizer setTranslation:CGPointMake(0, 0) inView:self];
            
        }
        
        if(_lengthorgX<_lengthorgY)
        {
            _lengthView.frame=CGRectMake(_lengthorgX, lengthView_y, _lengthorgY-_lengthorgX, lengthView_h);
        }else
        {
            _lengthView.frame=CGRectMake(_lengthorgY
                                         , lengthView_y, _lengthorgX-_lengthorgY, lengthView_h);
        }
        
        CGFloat  min_x = MIN(_lfview.center.x, _rgview.center.x) - self.HDview_h / 2.0;
        CGFloat  max_x = MAX(_lfview.center.x, _rgview.center.x) - self.HDview_h / 2.0;
        //            NSLog(@"____%f___%f",(min_x / Slider_length),(max_x / Slider_length));
        
        NSInteger minx = (NSInteger)(round(min_x / Slider_length));
        NSInteger maxx = (NSInteger)(round(max_x / Slider_length));
        
        //            NSLog(@"____%ld___%ld",minx,maxx);
        
        if (self.sliderValueChange) {
            
            if (maxx == count + 1 || ((bottomLength == 1) && (maxx == count))) {
                self.sliderValueChange( minx,maxx , YES);
            } else {
                self.sliderValueChange( minx,maxx , NO);
            }
        }

    }else if(recognizer.state == UIGestureRecognizerStateEnded){
        
        float origin_x = view.center.x;
        
        int a = (origin_x-self.HDview_h/2.0)/Slider_length;
        
        float b = origin_x - (a * Slider_length) - self.HDview_h/2.0;
        
        if ( b > (Slider_length/2.0) ) {
            origin_x = (a+1)*Slider_length + self.HDview_h/2.0;
        }else{
            origin_x = (a)*Slider_length + self.HDview_h/2.0;
        }
        
        if (_lfview==view) {
            _lengthorgX = origin_x;
        }else{
            _lengthorgY = origin_x;
        }
        
        if (fabs( _lengthorgX - _lengthorgY) < (Slider_length / 2.0)) {
            if ( _lengthorgX == self.HDview_h / 2.0 ) {
                _lengthorgX = _lengthorgX + Slider_length;
            } else {
                _lengthorgX = _lengthorgX - Slider_length;
            }
        }
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{            
            [_lfview setCenter:CGPointMake(_lengthorgX, view.centerY)];
            [_rgview setCenter:CGPointMake(_lengthorgY, view.centerY)];
            
            if(_lengthorgX<_lengthorgY)
            {
                _lengthView.frame=CGRectMake(_lengthorgX, lengthView_y, _lengthorgY-_lengthorgX, lengthView_h);
            }else
            {
                _lengthView.frame=CGRectMake(_lengthorgY, lengthView_y, _lengthorgX-_lengthorgY, lengthView_h);
            }
        } completion:^(BOOL finished) {
            
            CGFloat  min_x = MIN(_lfview.center.x, _rgview.center.x) - self.HDview_h / 2.0;
            CGFloat  max_x = MAX(_lfview.center.x, _rgview.center.x) - self.HDview_h / 2.0;
            NSInteger minx = (NSInteger)(round(min_x / Slider_length));
            NSInteger maxx = (NSInteger)(round(max_x / Slider_length));
            if (self.sliderValueChange) {
                if (maxx == count + 1 || ((bottomLength == 1) && (maxx == count))) {
                    self.sliderValueChange( minx,maxx , YES);
                } else {
                    self.sliderValueChange( minx,maxx , NO);
                }
            }
        }];
            
    }

}

- (void)setMin:(NSInteger)min max:(NSInteger)max {
    
    [_lfview setCenterX:(min * Slider_length + self.HDview_h / 2.0)];
    [_rgview setCenterX:(max * Slider_length + self.HDview_h / 2.0)];
    _lengthView.frame=CGRectMake(_lfview.centerX, lengthView_y, _rgview.centerX-_lfview.centerX, lengthView_h);
    _lengthorgX = _lfview.centerX;
    _lengthorgY = _rgview.centerX;
    
    if (self.sliderValueChange) {
        if (max == count + 1 || ((bottomLength == 1) && (max == count))) {
            self.sliderValueChange( min,max , YES);
        } else {
            self.sliderValueChange( min,max , NO);
        }
    }
    
}

@end
